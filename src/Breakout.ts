
import {Brick} from './Brick';
import {Player} from './Player';
import {Ball} from './Ball';

export module Breakout
{

    export class BreakoutGame{

        game : Phaser.Game;

        background : any;

        bricks : Phaser.Group;

        player : Player;
        ball   : Ball;

        shootkey : Phaser.Key;

        constructor() {
            document.writeln("Hello, World! from aaser 0  caaiano-" + Phaser.VERSION);

            this.game = new Phaser.Game(1280,520, Phaser.AUTO, 'game', 
            {
                    preload : this.preload,
                    create  : this.create,
                    update  : this.update
            })
        }
        
        preload()
        {
             console.log("[BreakoutGame] Loading Assets");

             this.game.load.atlas('atlas','assets/atlas.png','assets/atlas.json');

             console.log("[BreakoutGame] Assets aaLoaded");
        }
        create()
        {
            this.game.physics.startSystem(Phaser.Physics.ARCADE);

            //this.game.physics.arcade.checkCollision.down = false;

            //------ Bricks Creation 
            this.bricks = this.game.add.group();
            
            for(var i = 0 ; i < 5 ; i++)
            {
                for(var x = 0 ; x < 5 ; x++)
                {
                    let brick = new Brick( this.game , 120 + (x*35), 100 + (i*52) , 1);
                    this.bricks.add(brick);

                }      
            }

            this.bricks.enableBody = true;
            this.bricks.physicsBodyType = Phaser.Physics.ARCADE;


            //------ End Bricks Creation 
            

            //-- Create Player 
            this.player = new Player(this.game);
            this.game.add.existing(this.player);

            //-- End Create Player
            //-- Create Ball
            this.ball = new Ball(this);
            this.game.add.existing(this.ball);
            //-- End Create Ball

            //this.shootkey = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            
        }
        update()
        {
            //console.log(this.game.input.x);

            this.game.physics.arcade.collide(
                this.ball,this.player,
                ()=>{ this.ball.onHitPlayer; },
                null,this);

        }
        launchBall() 
        {
            console.log("pepe");
       
        }
    }


}


