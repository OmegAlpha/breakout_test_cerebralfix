
export class Brick extends Phaser.Sprite
{
    game : Phaser.Game;
    Duration : number;

    constructor(game :Phaser.Game , x:number,y:number , duration :number)
    {
        super(game,x,y,'atlas','brick_'+duration+'_1.png');
        this.anchor.set(0.5);
        this.Duration = duration;


        this.game.physics.arcade.enable(this);

        this.body.bounce.set(1);
        this.body.immovable = false;

        console.log("Ladrillo Creado");
    }

}