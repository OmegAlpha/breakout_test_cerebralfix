

export class Player extends Phaser.Sprite
{
    game : Phaser.Game;

    KEY_SPACEBAR : Phaser.Key;
    CURSORS      : Phaser.CursorKeys;
    
    previousX    : number;
    movementVelocity : number;

    constructor(game :Phaser.Game)
    {
        

        super(game,game.world.centerX, 500, 'atlas', 'paddle_big.png');

        this.game = game;   

        this.anchor.set(0.5);
        this.game.physics.enable(this,Phaser.Physics.ARCADE);
        this.body.collideWorldBounds = true;
        this.body.bounce.set(1);
        this.body.immovable = true; 

        console.log("[Player] Player Created");
    }

    update()
    {
         this.previousX = this.x;         
         this.x = this.game.input.x;

         this.movementVelocity = this.x - this.previousX;

         //console.log(this.movementVelocity);
    }

}
