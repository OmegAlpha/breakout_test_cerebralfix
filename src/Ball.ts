import {Breakout} from './Breakout';

export class Ball extends Phaser.Sprite
{
    game : Phaser.Game;
    breakout : Breakout.BreakoutGame;

    constructor(breakout : Breakout.BreakoutGame)
    {


        super(breakout.game,breakout.game.world.centerX, 485 ,'atlas','ball_1.png');

        this.breakout = breakout;
        this.game = breakout.game;

        this.anchor.set(0.5);
        this.checkWorldBounds = true;

        this.game.physics.enable(this,Phaser.Physics.ARCADE);

        this.body.collideWorldBounds = true;
        this.body.bounce.set(1);
        this.body.immovable = false;

        

        this.launch();
    }

    launch()
    {
        console.log("[Ball] Launch");

        this.body.velocity.y = -300;
        this.body.velocity.x = -75;

    }
    onHitPlayer()
    {
        console.log("choque");
        this.body.velocity.x += this.breakout.player.movementVelocity;
    }

}